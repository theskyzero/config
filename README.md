[toc]

# 公共配置库

---



# 介绍

保存自己的idea公共配置等省的换个电脑还要配一堆东西~

基于2020.2.1版本，使用中文语言插件

# IDEA使用

文件 => 导入导出 => 设置存储库

# 配置项

文件 => 设置（Ctrl +Alt+S ）

## 外观和行为

### 外观

```yaml
主题: Darcula
```

### 菜单和工具栏

### 常规

```yaml
推出IDE之前确认: true
项目: 
	启动时重新打开项目: false
自动保存: 
	切换到其他应用程序时保存文件: true
	保存前备份文件: true
	切换到IDE窗口或打开编辑器选项卡时同步外部变更: true
```

#### 密码

```yaml
在KeePass中: true
```

#### 更新

```yaml
在通道中检查更新: false
```

## 快捷键

```yaml
Eclipse: true
```

## 编辑器

### 编辑器

#### 自动导入

```yaml
Java:
	快速添加清晰得导入: true
	即时优化导入: true
```

#### 代码完成

```yaml
匹配大小写: false
输入时显示建议: true
机器学习辅助补全:
	基于机器学习排名补全建议: true
		Java: true
参数信息:
	完成时显示参数名称提示: true
	自动弹出在(ms): 1000ms
	显示完整得签名: true
```

#### 代码折叠

#### 后缀完成

#### 外观

```yaml
显示行号: true
显示方法分隔符: true
```

#### 智能键

```yaml
插入成对括号: true
插入双引号: true
使用“驼峰”字: false	# 双击选中变量而非单词
```

#### 编辑器选项卡

```yaml
显示文件拓展名: true
用星号标记已修改的标签: true	# * 标记未保存文件
```

### 代码风格

```yaml
# Default IDE
General:
	行分隔符(新文件): Unix和OS X()	# CF， linux格式换行符，建议使用
```

#### Java

### 检查

```yaml
Profile: Default IDE
Java: ture	# 暂时勾选所有的java检查，有助于日常编码规范
```

### 文件和代码模板

```yaml
# 方案 Default 
包含:
	File Header: 	#  类注释
```

> File Header
>
> ```
> /**
> *@author ${USER}
> *@create ${YEAR}/${MONTH}/${DAY}
> **/
> ```

### 文件编码

```yaml
全局编码: UTF-8
项目编码: UTF-8
属性文件:
	属性文件的默认编码: UTF-8
	透明的原生到ASCII转换: true
```

### 版权

开源项目可设置

### 嵌入提示

```yaml
显示以下项的提示: all
	Java: true
```

#### Java

```yaml
参数提示:
	为以下对象显示参数提示: all
方法链:
隐式类型:
<html>注释</html>:
	显示提示:
		外部注释: true
Code Vision
```

## 插件

**用户下载**

```yaml
Free MyBatis plugin
JnitGenerator V2.0
Maven Helper
Lombok
RestfulToolkit
Alibaba Java Coding Guidelines: 	# java开发必备
QA-Plug
QA-FindBugs
CheckStyle-IDEA
VisualVm Launcher

Chinese（Simplified）Language Pack EAP:
IDE Features Traniner
Key Promoter X
Rainbow Brackets
```

**默认启用**

```yaml
Android: false
Built Tools: 
	Gradle
	Gradle Extension
	Maven
	Maven Extension
Code Converage: all
	Code Converage for java
	Emma
Database:
	Database Tools and Sql
Deployment:
	Docker: true
	Ftp/SFTP COnnectivity: true
HTML: all
IDE Settings: all
JavaScript Frameworks and Tools: all
JVM Frameworks: all
Languages: all
Other Tools: all
Plugin Development: false
Style Sheets: all
Swing: false
Template Languages: all
Test Tools:
	JUnit
	TestNG
Version Control: false
Version Controls:
	GIt
	Subversion
其他工具: all

```

## 版本控制

### 提交对话框

```yaml
置入非空提交的注释: 
在Commit之前:
	格式化代码: true
	重新整理代码: true
	优化导入: true
	执行代码分析: true
	检查TODO: true
```

#### Git

```yaml
即将提交CRLF行分隔符时发出警告: true
```

## 构建、执行、部署

### 构建工具

#### Maven

```yaml
打印异常堆栈追踪: true
```

##### 导入

```yaml
自动下载: 
	源: true
```

##### 运行程序

```yaml
跳过测试: true
```

### 编译器

```yaml
自动构建项目: true
并行编译独立模块: true
依赖关系更改时重建模块: true
```

## 工具

### 图

```yaml
Java: all	# uml图
```

## 其他设置

### JUnit Generator

**Properties**

```yaml
# Properties
Output Path: ${SOURCEPATH}/../../test/java/${PACKAGE}/${FILENAME}
```

**JUnit 4**

```yaml
package $entry.packageName; 
```

### Lombok plugin

```yaml
enable: all
```

